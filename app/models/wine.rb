class Wine < ApplicationRecord
  attr_accessor :name
  validates :name, presence: true, length: { maximum: 50 }
end
