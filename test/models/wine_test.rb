require 'test_helper'

class WineTest < ActiveSupport::TestCase
  def setup
    @wine = Wine.new(name: "Example Wine")
  end

  test 'should be valid' do
    assert @wine.valid?
  end

  test 'name should be present' do
    @wine.name = ' '
    assert_not @wine.valid?
  end

  test 'name should not be too long' do
    @wine.name = 'a' * 51
    assert_not @wine.valid?
  end
end
